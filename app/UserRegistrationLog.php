<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRegistrationLog extends Model
{
    protected $table = 'users';

    static public function getUserLog(User $user){

        $userLog = [
            'userName' => $user->name,
            'userRegisterDate' => $user->created_at
        ];

        return $userLog;
    }
}
